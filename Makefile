# makefile

VER = $(shell date +%y.%m.%d)

help:
	@echo "make release     Bundle files for release."
	@echo "make dnd         Just make a DnD 5e release."
	@echo "make pathfinder  Just make a 3.5 release."
	@echo "make raider      Just make Dungeon Raider release."
	@echo "make clean       Remove temporary build files."

release: dnd pathfinder raiders

dnd:
	@mkdir dnd-character_sheet-$(VER)
	@cp character5e.ods \
	LICENSE README.md \
	ChangeLog \
	OGLv1.0a.txt \
	dnd-character_sheet-$(VER)
	@zip dnd-character_sheet-$(VER).zip -r \
	dnd-character_sheet-$(VER)

pathfinder:
	@mkdir pathfinder-character_sheet-$(VER)
	@cp character.ods \
	LICENSE README.md \
	ChangeLog \
	OGLv1.0a.txt \
	pathfinder-character_sheet-$(VER)
	@zip pathfinder-character_sheet-$(VER).zip -r \
	pathfinder-character_sheet-$(VER)

raiders:
	@mkdir dungeonRaiders-character_sheet-$(VER)
	@cp raiders/character-raiders.ods \
	raiders/Dungeon-Raiders_revised-edition-d03.epub \
	LICENSE README.md \
	ChangeLog \
	OGLv1.0a.txt \
	dungeonRaiders-character_sheet-$(VER)
	@zip dungeonRaiders-character_sheet-$(VER).zip -r \
	dungeonRaiders-character_sheet-$(VER)

clean:
	@rm -rf *.zip
	@rm -rf pathfinder-character_sheet-*
	@rm -rf dnd-character_sheet-*
	@rm -rf dungeonRaiders-character_sheet-*
