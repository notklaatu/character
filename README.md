# Pathfinder and 5e Character Sheets

The speadsheet opens and functions in the open source applications [LibreOffice](http://libreoffice.org), [OpenOffice](http://openoffice.org), and [GNUmeric](http://gnumeric.org), but may also work in other applications that use the ``.ods`` format.

Its goal is to mimic as closely as possible the official **Paizo** and **Wizards of the Coast** character sheet design.

The spreadsheet is designed with a minimal number of auto functions, leaving you with the flexibility to fill it in the way the works best for you, but freeing you from mundane calculations. Automatic calculations are kept to a minimum, so you will still need the Pathfinder Core Rulebook or D&D Player's Handbook on hand to get a list of class skills and saving throw values, and so on.

* White cells are cells you can safely fill in.
* Gray or black cells are either labels or auto-fill cells.

There are two sheets in the document, corresponding roughly to the original paper-based character sheet, with some minor variation for the sake of layout.


## License

The document itself is [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

The information within the document (terminology, game concepts, and so on) is Open Game License.

The Pathfinder character sheet uses trademarks and/or copyrights owned by Paizo Inc., which are used under Paizo's Community Use Policy. I am expressly prohibited from charging you to use or access this content. This character sheet is not published, endorsed, or specifically approved by Paizo Inc. For more information about Paizo's Community Use Policy, please visit http://paizo.com/communityuse. For more information about Paizo Inc. and Paizo products, please visit http://paizo.com.

The D&D character sheet uses the DMs Guild logo, designating it as community-created content. DUNGEONS & DRAGONS, D&D, Wizards of the Coast, and all other Wizards of the Coast product names, and their respective logos, are trademarks of Wizards of the Coast in the USA and other countries. This material is protected under the copyright laws of the Tyrannical States of America.


## Bugs

If you find bugs or have suggestions, feel free to let me know.

